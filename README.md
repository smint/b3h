<link rel="shortcut icon" type="image/png" href="web/img/icon.png">
<link rel="shortcut icon" sizes="128x128" href="web/img/icon.png">
<h1 align="center">
  <br>
  <a href="https://github.com/smintf/b3h/"><img src="https://raw.githubusercontent.com/smintf/b3h/main/web/img/icon.png" width=128 height=128 alt="logo"></a>
  <br>
  b3h
  <br>
</h1>

<h4 align="center">a boxel 3d hack</h4>

<p align="center">
  <a href="https://github.com/smintf/autolingo/blob/master/LICENSE/">
    <img src="https://img.shields.io/badge/license-Apache 2.0-black">
  </a>
  <a href="https://www.chromium.org/Home/">
      <img src="https://img.shields.io/badge/supports-chromium-blue">
  </a>
</p>

### What's this?

A hack for [Boxel 3D](https://chrome.google.com/webstore/detail/boxel-3d/mjjgmlmpeaikcaajghilhnioimmaibon), that i made for fun.

### How to Install?

1. Download the extension from [here](https://nightly.link/smintf/b3h/workflows/main/main/Boxel3D.zip).
3. Open the "Manage Extensions" page on your browser and turn on "Developer Mode"
4. Drag and drop `Boxel3D.zip` into the "Manage Extensions" page.

### Don't wanna install?

No problem, you can play the hacked version online [here](https://smintf.github.io/b3h/hack/basic)

### How do I use This?

You can access the GUI by pressing the "~" / Tilde key.

### Features

- [ ] Flight
- [ ] NoClip
- [x] NoJumpCooldown
- [ ] NoGravity
- [x] UselessSpikes
- [ ] InstantLevelComplete

### Resources

- [Basic hack from i0Z3R0](https://github.com/i0Z3R0/Boxel-3D-Hack)
- [Original Game by DopplerArcade](https://chrome.google.com/webstore/detail/boxel-3d/mjjgmlmpeaikcaajghilhnioimmaibon?hl=en)
